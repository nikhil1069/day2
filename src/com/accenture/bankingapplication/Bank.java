package com.accenture.bankingapplication;


public class Bank {

@Override
	public String toString() {
		return "Bank [bid=" + bid + ", bankName=" + bankName + ", bankBranch=" + bankBranch + ", noOfCustomers="
				+ noOfCustomers + "]";
	}

private int bid;
private String bankName;
private String bankBranch;
private int noOfCustomers;

 // empty constructor
public Bank() {
System.out.println("empty object created...");
}

 // parameterized constructor
public Bank(int bid, String bankName, String bankBranch, int noOfCustomers) {
this.bid = bid;
this.bankName = bankName;
this.bankBranch = bankBranch;
this.noOfCustomers = noOfCustomers;
}

 // parameterized constructor
public Bank(int bid, String bankBranch ) {
	this.bid = bid;
	this.bankBranch = bankBranch;
}

 // parameterized constructor
public Bank(int bid, String bankName, int noOfCustomers) {
	this.bid = bid;
	this.bankName = bankName;
	this.noOfCustomers = noOfCustomers;
}


public int getBid() {
	return bid;
}

public void setBid(int bid) {
	this.bid = bid;
}

public String getBankName() {
	return bankName;
}

public void setBankName(String bankName) {
	this.bankName = bankName;
}

public String getBankBranch() {
	return bankBranch;
}

public void setBankBranch(String bankBranch) {
	this.bankBranch = bankBranch;
}

public int getNoOfCustomers() {
	return noOfCustomers;
}

public void setNoOfCustomers(int noOfCustomers) {
	this.noOfCustomers = noOfCustomers;
}

}