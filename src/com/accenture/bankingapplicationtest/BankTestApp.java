package com.accenture.bankingapplicationtest;

import com.accenture.bankingapplication.Bank;

public class BankTestApp {
public static void main(String[] args) {
System.out.println("Bank demo...");
Bank b1 = new Bank();
Bank b2 = new Bank(1, "SBI","Hyderabad", 1200);
Bank b3 = new Bank(2, "Pune");
Bank b4 = new Bank(3, "HDFC",1840);

System.out.println(b1);
System.out.println("------------------------");

System.out.println(b2);
System.out.println("------------------------");

System.out.println(b3);
b3.setBankName("PNB");
b3.setNoOfCustomers(41000);
System.out.println("------------------------");

System.out.println(b3);
System.out.println("------------------------");
System.out.println(b2.getNoOfCustomers());
System.out.println(b2.getBankName());
System.out.println("------------------------");

System.out.println(b4);

}
}
